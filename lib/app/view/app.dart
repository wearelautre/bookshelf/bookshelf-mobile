import 'package:authentication_repository/authentication_repository.dart';
import 'package:bookshelf_mobile/app/bloc/app_bloc.dart';
import 'package:bookshelf_mobile/books/view/book_page.dart';
import 'package:bookshelf_mobile/login/cubit/login_cubit.dart';
import 'package:bookshelf_mobile/scanner/scanner.dart';
import 'package:bookshelf_mobile/settings/view/settings_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import 'home.dart';

class App extends StatelessWidget {
  App({
    super.key
  });

  final _authenticationRepository = AuthenticationRepository();

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: _authenticationRepository,
      child: MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => AppBloc(authenticationRepository: _authenticationRepository)),
          BlocProvider(create: (_) => LoginCubit(_authenticationRepository)),
        ],
        child: const AppView(),
      ),
    );
  }
}

class AppView extends StatelessWidget {
  const AppView({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      routeInformationParser:
      BookshelfAppRouter.returnRouter(false).routeInformationParser,
      routerDelegate:
      BookshelfAppRouter.returnRouter(false).routerDelegate,
    );
  }
}

class RouteNameConstants {
  static const String homeRouteName = 'home';
  static const String scannerRouteName = 'scanner';
  static const String bookDetailsRouteName = 'bookDetails';
  static const String settingsRouteName = 'settings';
}

class BookshelfAppRouter {
  static GoRouter returnRouter(bool isAuth) {
    GoRouter router = GoRouter(
      routes: [
        GoRoute(
          name: RouteNameConstants.homeRouteName,
          path: '/',
          pageBuilder: (context, state) {
            return const MaterialPage(child: Home());
          },
        ),
        GoRoute(
          name: RouteNameConstants.scannerRouteName,
          path: '/scanner',
          pageBuilder: (context, state) {
            return const MaterialPage(child: ScannerPage());
          },
        ),
        GoRoute(
          name: RouteNameConstants.settingsRouteName,
          path: '/settings',
          pageBuilder: (context, state) {
            return const MaterialPage(child: SettingsPage());
          },
        ),
        GoRoute(
          name: RouteNameConstants.bookDetailsRouteName,
          path: '/book/:isbn',
          pageBuilder: (context, state) {
            return MaterialPage(
              child: BookPage(
                isbn: state.queryParameters["isbn"],
              ),
            );
          },
        ),
      ],
      errorPageBuilder: (context, state) {
        return const MaterialPage(child: Home());
      },
    );
    return router;
  }
}
