import 'package:bookshelf_mobile/app/bloc/app_bloc.dart';
import 'package:bookshelf_mobile/app/view/app.dart';
import 'package:bookshelf_mobile/login/cubit/login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

/// {@template bookToRead_page}
/// A [StatelessWidget] which is responsible for providing a super text
/// {@endtemplate}
class Home extends StatelessWidget {
  /// {@macro bookToRead_page}
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Bookshelf'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.qr_code_scanner),
            tooltip: 'Go to scanner',
            onPressed: () {
              GoRouter.of(context)
                  .pushNamed(RouteNameConstants.scannerRouteName);
            },
          ),
          IconButton(
            icon: const Icon(Icons.settings),
            tooltip: 'Go to settings',
            onPressed: () {
              GoRouter.of(context)
                  .pushNamed(RouteNameConstants.settingsRouteName);
            },
          ),
          BlocListener<LoginCubit, LoginState>(
            listener: (context, state) {
              if(state.status == LoginStatus.failed) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content:
                      Text(state.errorMessage ?? 'Authentication Failure'),
                    ),
                  );
              }
            },
            child: BlocConsumer<AppBloc, AppState>(
              listener: (context, state) {

              },
              builder: (context, state) {
                if(state.status == AppStatus.authenticated) {
                  return IconButton(
                    icon: const Icon(Icons.logout),
                    tooltip: 'Logout',
                    onPressed: () => context.read<LoginCubit>().logout(),
                  );
                } else {
                  return IconButton(
                    icon: const Icon(Icons.person),
                    tooltip: 'Login',
                    onPressed: () => context.read<LoginCubit>().login(),
                  );
                }
              },
            )
          ),
        ],
      ),
      body: const Center(
        child: Text(
          'This is the home page',
          style: TextStyle(fontSize: 24),
        ),
      ),
    );
  }
}
