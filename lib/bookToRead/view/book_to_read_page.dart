import 'package:flutter/material.dart';

/// {@template bookToRead_page}
/// A [StatelessWidget] which is responsible for providing a super text
/// {@endtemplate}
class BookToReadPage extends StatelessWidget {
  /// {@macro bookToRead_page}
  const BookToReadPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Text("bookToRead");
  }
}
