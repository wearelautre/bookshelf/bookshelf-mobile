import 'package:bookshelf_mobile/books/data/models/book.dart';
import 'package:bookshelf_mobile/books/data/repository/book_repository.dart';
import 'package:bookshelf_mobile/books/exceptions/book_not_found_exception.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'book_state.dart';

class BookCubit extends Cubit<BookState> {
  final BooksRepository booksRepository;

  BookCubit({required this.booksRepository}) : super(BookInitial());

  void fetchBookByIsbn(String isbn) {
    booksRepository
        .fetchBookByIsbn(isbn)
        .then((book) => {
              if (book != null)
                {emit(BookLoaded(book: book))}
              else
                {emit(BookError(error: 'FAILED'))}
            })
        .catchError((error) => {
              if (error is BookNotFoundException)
                {emit(BookNotFound(isbn: error.isbn))}
            });
  }

  void createTask() {
    debugPrint('createTask');
  }

  markAsRead() {
    debugPrint('markAsRead');
  }
}
