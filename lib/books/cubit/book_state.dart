part of 'book_cubit.dart';

@immutable
abstract class BookState {}

class BookInitial extends BookState {}

class BookLoading extends BookState {}

class BookError extends BookState {
  final String error;

  BookError({required this.error});
}

class BookNotFound extends BookState {
  final String isbn;

  BookNotFound({required this.isbn});
}

class BookLoaded extends BookState {
  final Book book;

  BookLoaded({required this.book});
}
