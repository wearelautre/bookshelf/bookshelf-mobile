import 'package:dio/dio.dart';

class DioClient {
  final dio = createDio();

  DioClient._internal();

  static final _singleton = DioClient._internal();

  factory DioClient() => _singleton;

  static Dio createDio() {
    var dio = Dio(
      BaseOptions(),
    );

    return dio;
  }
}
