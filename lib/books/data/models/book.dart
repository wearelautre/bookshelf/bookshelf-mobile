class Book {
  String? isbn;
  String? title;
  int? tome;
  int? tomeCycle;
  String? series;
  String? cycle;

  Book({this.isbn, this.title});

  Book.fromJson(Map<String, dynamic> bookJson) {
    isbn = bookJson['isbn'];
    title = bookJson['title'];
    if (bookJson['series'] != null) {
      Map<String, dynamic> seriesJson = bookJson['series'];
      series = seriesJson['name'];
      tome = seriesJson['tome'];
      if (seriesJson['seriesCycle'] != null) {
        Map<String, dynamic> seriesCycleJson = seriesJson['seriesCycle'];
        cycle = seriesCycleJson['name'];
        tomeCycle = seriesCycleJson['tome'];
      }
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['isbn'] = isbn;
    data['title'] = title;
    return data;
  }
}
