import 'package:bookshelf_mobile/books/data/models/book.dart';

import '../services/providers/books_providers.dart';

class BooksRepository {
  final BooksProviders providers;

  BooksRepository(this.providers);

  Future<Book?> fetchBookByIsbn(String isbn) async {
    return await providers.fetchBookByIsbn(isbn);
  }
}
