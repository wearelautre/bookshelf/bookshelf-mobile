import 'package:bookshelf_mobile/books/data/models/book.dart';
import 'package:bookshelf_mobile/books/exceptions/book_not_found_exception.dart';
import 'package:commons/settings_enum.dart';
import 'package:commons/utils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../dio_client.dart';

class BooksProviders {
  final DioClient _client = DioClient();

  BooksProviders();

  Future<Book?> fetchBookByIsbn(String isbn) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String domain = SharedPreferencesUtils.getPreferenceValue(
        prefs,
        SettingKeys.domain,
        null,
      );

      Response response = await _client.dio.get("http://$domain/books/$isbn");
      debugPrint("SAMPLE_DATA : ${response.data}");
      return Book.fromJson(response.data);
    } on DioError catch (e) {
      if (e.response!.statusCode == 404) {
        throw BookNotFoundException(isbn: isbn);
      }
      debugPrint("ERROR : $e");
      return null;
    }
  }
}
