class BookNotFoundException implements Exception {
  final String isbn;

  BookNotFoundException({required this.isbn});
}
