import 'package:bookshelf_mobile/books/cubit/book_cubit.dart';
import 'package:bookshelf_mobile/books/data/repository/book_repository.dart';
import 'package:bookshelf_mobile/books/data/services/providers/books_providers.dart';
import 'package:bookshelf_mobile/books/view/book_view.dart';
import 'package:bookshelf_mobile/task/cubit/task_cubit.dart';
import 'package:bookshelf_mobile/task/data/repository/task_repository.dart';
import 'package:bookshelf_mobile/task/data/services/providers/tasks_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// booksRepository.fetchBookByIsbn(bloc.state);

class BookPage extends StatelessWidget {
  final bookCubit = BookCubit(booksRepository: BooksRepository(BooksProviders()));
  final taskCubit = TaskCubit(tasksRepository: TasksRepository(TasksProviders()));

  BookPage({required isbn, super.key}) {
    bookCubit.fetchBookByIsbn(isbn);
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => bookCubit),
        BlocProvider(create: (_) => taskCubit),
      ],
      child:  Scaffold(
        appBar: AppBar(title: const Text('Details du livre')),
        body: const BookView(),
      ),
    );
  }
}
