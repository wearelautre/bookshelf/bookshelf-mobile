import 'package:bookshelf_mobile/books/cubit/book_cubit.dart';
import 'package:bookshelf_mobile/task/cubit/task_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

// booksRepository.fetchBookByIsbn(bloc.state);

class BookView extends StatelessWidget {
  const BookView({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BookCubit, BookState>(
      builder: (context, state) {
        if (state is BookLoading || state is BookInitial) {
          return const CircularProgressIndicator();
        } else if (state is BookLoaded) {
          final book = (state).book;
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(book.isbn ?? "Unknown Isbn"),
                Text(book.title ?? "Unknown Title"),
                Text((book.tome ?? "Unknown Tome").toString()),
                Text((book.tomeCycle ?? "Unknown Tome").toString()),
                Text(book.series ?? "Unknown Series"),
                Text(book.cycle ?? "Unknown Cycle"),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Spacer(
                      flex: 2,
                    ),
                    ElevatedButton(
                      onPressed: () => context.read<BookCubit>().markAsRead(),
                      child: const Text("Marqué comme lu"),
                    ),
                    const Spacer(),
                    ElevatedButton(
                      onPressed: () => context.read<BookCubit>().markAsRead(),
                      child: const Text("Marqué comme vendu"),
                    ),
                    const Spacer(
                      flex: 2,
                    ),
                  ],
                ),
              ],
            ),
          );
        } else if (state is BookError) {
          return Text((state).error);
        } else if (state is BookNotFound) {
          return BlocListener<TaskCubit, TaskState>(
            listener: (context, state) {
              if (state is TaskError) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content: Text(state.error),
                    ),
                  );
              } else if (state is TaskCreated) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content: Text("Une tache d'ajout du livre ${state.task.extra} à été créée"),
                    ),
                  );
                GoRouter.of(context).pop();
              }
            },
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("Le livre n'est pas présent dans la bibliothèque"),
                  ElevatedButton(
                    onPressed: () =>
                        context.read<TaskCubit>().createTask(state.isbn),
                    child: const Text("Demander l'ajout dans la bibliothèque"),
                  )
                ],
              ),
            ),
          );
        } else {
          return const Text("BIG ERROR !!");
        }
      },
      listener: (context, state) {},
    );
  }
}
