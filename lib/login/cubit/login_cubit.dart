import 'package:authentication_repository/authentication_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit(this._authenticationRepository) : super(const LoginState());

  final AuthenticationRepository _authenticationRepository;

  Future<void> login() async {
    try {
      await _authenticationRepository.login();
      emit(
        state.copyWith(
          status: LoginStatus.success,
          errorMessage: null,
        ),
      );
    } on LoginFailure catch (e) {
      emit(
        state.copyWith(
          status: LoginStatus.failed,
          errorMessage: e.message,
        ),
      );
    } catch (e) {
      emit(
        state.copyWith(
          status: LoginStatus.failed,
          errorMessage: null,
        ),
      );
    }
  }

  Future<void> logout() async {
    try {
      await _authenticationRepository.logOut();
      emit(
        state.copyWith(
          status: LoginStatus.unlogged,
          errorMessage: null,
        ),
      );
    } catch (e) {
      emit(
        state.copyWith(
          status: LoginStatus.unlogged,
          errorMessage: e.toString(),
        ),
      );
    }
  }
}
