part of 'login_cubit.dart';

enum LoginStatus {
  unlogged,
  success,
  failed,
}

class LoginState extends Equatable {
  const LoginState({
    this.status = LoginStatus.unlogged,
    this.errorMessage,
  });

  final String? errorMessage;
  final LoginStatus status;

  @override
  List<Object> get props => [status];

  LoginState copyWith({
    LoginStatus? status,
    String? errorMessage,
  }) {
    return LoginState(
      status: status ?? this.status,
      errorMessage: errorMessage ?? this.errorMessage,
    );
  }
}
