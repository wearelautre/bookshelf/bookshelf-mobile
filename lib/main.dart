import 'package:bookshelf_mobile/app/view/app.dart';
import 'package:bookshelf_mobile/preconfig_app.dart';
import 'package:commons/settings_enum.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();

  List<SettingKeys> mandatorySettings = [
    SettingKeys.domain,
    SettingKeys.keycloakClientSecret,
    SettingKeys.keycloakRealm,
    SettingKeys.keycloakUrl,
    SettingKeys.keycloakClientId,
  ];

  isConfigured(SharedPreferences prefs) {
    return mandatorySettings
        .every((element) => prefs.getString(element.token) != null);
  }

  // Build the website
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: prefs,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        } else if (snapshot.hasData) {
          SharedPreferences prefs = snapshot.data as SharedPreferences;
          if (!isConfigured(snapshot.data!)) {
            return PreconfigApp(
              prefs: prefs,
              onConfigurationUpdate: () => setState(() {}),
            );
          }
          return App();
        } else {
          return const Center(
            child: SizedBox(
              height: 150,
              width: 150,
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}
