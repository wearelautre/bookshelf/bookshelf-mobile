import 'package:commons/input_text_widget.dart';
import 'package:commons/settings_enum.dart';
import 'package:commons/utils.dart';
import 'package:commons/widget/input_dialog_option.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreconfigApp extends StatefulWidget {
  final SharedPreferences prefs;
  final Function() onConfigurationUpdate;

  const PreconfigApp({required this.prefs, super.key, required void Function() this.onConfigurationUpdate});

  @override
  PreconfigAppState createState() => PreconfigAppState();
}

class ConfigInputContainerValues {
  String title;
  String description;
  String hint;
  SettingKeys settingKey;
  bool isPassword = false;

  ConfigInputContainerValues({
    required this.title,
    required this.description,
    required this.hint,
    required this.settingKey,
    this.isPassword = false,
  });
}

class PreconfigAppState extends State<PreconfigApp> {
  List<ConfigInputContainerValues> list = [
    ConfigInputContainerValues(
      title: "Domain",
      description: "Description Domain",
      hint: "bookshelf.my-domain.bzh",
      settingKey: SettingKeys.domain,
    ),
    ConfigInputContainerValues(
      title: "Auth server url (keycloak)",
      description: "Description keycloak url",
      hint: "auth.bookshelf.bzh",
      settingKey: SettingKeys.keycloakUrl,
    ),
    ConfigInputContainerValues(
      title: "Realm",
      description: "Description realm",
      hint: "bookshelf",
      settingKey: SettingKeys.keycloakRealm,
    ),
    ConfigInputContainerValues(
      title: "Client ID",
      description: "Description client ID",
      hint: "bookshelf_mobile",
      settingKey: SettingKeys.keycloakClientId,
    ),
    ConfigInputContainerValues(
      title: "Client secret",
      description: "Description client secret",
      hint: "mysupersecret",
      settingKey: SettingKeys.keycloakClientSecret,
      isPassword: true,
    )
  ];

  _configInputContainer(
    String title,
    String description,
    String hint,
    SettingKeys settingKey,
    bool isPassword,
  ) {
    return Container(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 30),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: Theme.of(context).textTheme.titleMedium,
              ),
              Text(
                description,
                style: Theme.of(context).textTheme.bodySmall,
                textAlign: TextAlign.left,
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
            child: InputTextConfiguration(
              key: Key(title),
              option: InputDialogOption(
                titre: title,
                inputHint: hint,
                isRequired: true,
                isPassword: isPassword
              ),
              onValidate: (res) {
                SharedPreferencesUtils.setPreferenceValue(
                  widget.prefs,
                  settingKey,
                  res.value,
                );
                widget.onConfigurationUpdate();
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(40, 25, 40, 30),
                child: Column(
                  children: [
                    Text(
                      "Configuration",
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 10),
                    Text(
                      "All the following configurations are mandatory you need to fill them up to proceed to the app",
                      style: Theme.of(context).textTheme.bodyMedium,
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
              ),
              ...list
                  .where(
                    (e) => widget.prefs.getString(e.settingKey.token) == null,
                  )
                  .map(
                    (e) => _configInputContainer(
                      e.title,
                      e.description,
                      e.hint,
                      e.settingKey,
                      e.isPassword,
                    ),
                  ),
            ],
          ),
        ),
      ),
    );
  }
}
