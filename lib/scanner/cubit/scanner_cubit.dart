import 'package:flutter_bloc/flutter_bloc.dart';

/// {@template scanner_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class ScannerCubit extends Cubit<String> {
  /// {@macro scanner_cubit}
  ScannerCubit() : super("");

  void newBarcodeScanned(String isbn) => emit(isbn);
}
