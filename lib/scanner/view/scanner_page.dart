import 'package:bookshelf_mobile/scanner/scanner.dart';
import 'package:flutter/material.dart';

/// {@template scanner_page}
/// A [StatelessWidget] which is responsible for providing a
/// [ScannerCubit] instance to the [ScannerView].
/// {@endtemplate}
class ScannerPage extends StatelessWidget {
  /// {@macro scanner_page}

  const ScannerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const ScannerView();
  }
}
