import 'package:commons/settings_enum.dart';
import 'package:commons/widget/input_dialog_option.dart';
import 'package:bookshelf_mobile/settings/widgets/settings_tile_input_text.dart';
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

class ConnectivitySettingsSection extends AbstractSettingsSection {
  const ConnectivitySettingsSection({super.key});

  @override
  Widget build(BuildContext context) {
    return SettingsSection(
      title: const Text('API connectivity'),
      tiles: [
        CommonInputTextSettingsTile(
          settingKey: SettingKeys.domain,
          icon: const Icon(Icons.domain),
          title: "Bookshelf domain",
          inputDialogOption: InputDialogOption(
            inputHint: "bookshelf.my-domain.bzh",
            titre: "Bookshelf domain",
            isRequired: true,
          ),
        ),
      ],
    );
  }
}
