import 'package:commons/settings_enum.dart';
import 'package:commons/widget/input_dialog_option.dart';
import 'package:bookshelf_mobile/settings/widgets/settings_tile_input_text.dart';
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

class KeycloakSettingsSection extends AbstractSettingsSection {
  const KeycloakSettingsSection({super.key});

  @override
  Widget build(BuildContext context) {
    return SettingsSection(
      title: const Text('Keycloak'),
      tiles: [
        CommonInputTextSettingsTile(
          settingKey: SettingKeys.keycloakUrl,
          title: "Auth server url",
          icon: const Icon(Icons.link),
          inputDialogOption: InputDialogOption(
            inputHint: "auth.bookshelf.bzh",
            titre: "Auth server url",
            isRequired: true,
          ),
        ),
        CommonInputTextSettingsTile(
          settingKey: SettingKeys.keycloakRealm,
          title: "Realm",
          icon: const Icon(Icons.text_fields),
          inputDialogOption: InputDialogOption(
            inputHint: "bookshelf",
            titre: "Realm",
            isRequired: true,
          ),
        ),
        CommonInputTextSettingsTile(
          settingKey: SettingKeys.keycloakClientId,
          title: "Client ID",
          icon: const Icon(Icons.text_fields),
          inputDialogOption: InputDialogOption(
            inputHint: "bookshelf_mobile",
            titre: "Client ID",
            isRequired: true,
          ),
        ),
        CommonInputTextSettingsTile(
          settingKey: SettingKeys.keycloakClientSecret,
          title: "Client Secret",
          icon: const Icon(Icons.lock),
          inputDialogOption: InputDialogOption(
            inputHint: "mysupersecret",
            titre: "Client secret",
            isRequired: true,
            isPassword: true,
          ),
        ),
      ],
    );
  }
}
