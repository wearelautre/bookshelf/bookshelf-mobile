import 'package:bookshelf_mobile/settings/view/sections/connectivity/settings_section_connectity.dart';
import 'package:bookshelf_mobile/settings/view/sections/keycloak/settings_section_keycloak.dart';
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

class SettingsView extends StatelessWidget {
  const SettingsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Settings')),
      body: const SettingsList(
        sections: [
          ConnectivitySettingsSection(),
          KeycloakSettingsSection(),
        ],
      ),
    );
  }
}
