import 'package:commons/settings_enum.dart';
import 'package:commons/utils.dart';
import 'package:commons/widget/input_dialog_option.dart';
import 'package:commons/widget/input_text_dialog_widget.dart';
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CommonInputTextSettingsTile extends AbstractSettingsTile {
  final SettingKeys settingKey;
  final InputDialogOption inputDialogOption;
  final String title;
  final Icon icon;

  const CommonInputTextSettingsTile({
    required this.settingKey,
    required this.title,
    required this.icon,
    required this.inputDialogOption,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return CommonInputTextWidget(
      preferenceKey: settingKey,
      title: title,
      icon: icon,
      inputDialogOption: inputDialogOption,
    );
  }
}

class CommonInputTextWidget extends StatefulWidget {
  final SettingKeys preferenceKey;
  final InputDialogOption inputDialogOption;
  final String title;
  final Icon icon;

  const CommonInputTextWidget({
    required this.preferenceKey,
    super.key,
    required this.title,
    required this.icon,
    required this.inputDialogOption,
  });

  @override
  CommonInputTextWidgetState createState() => CommonInputTextWidgetState();
}

class CommonInputTextWidgetState extends State<CommonInputTextWidget> {
  String? value;

  get _value {
    if (value == null) {
      return null;
    }
    if (widget.inputDialogOption.isPassword) {
      return const Text(
        "password set",
        style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),
      );
    }
    return Text(value!);
  }

  void loadSettings() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(
      () => value = SharedPreferencesUtils.getPreferenceValue(
        prefs,
        widget.preferenceKey,
        null,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    loadSettings();
  }

  @override
  Widget build(BuildContext context) {
    return SettingsTile.navigation(
      title: Text(widget.title),
      leading: widget.icon,
      value: _value,
      onPressed: (context) async {
        var res = await showDialog<InputDialogReturn>(
          context: context,
          builder: (BuildContext context) {
            return InputDialog(
              option: InputDialogOption(
                titre: widget.inputDialogOption.titre,
                inputHint: widget.inputDialogOption.inputHint,
                isRequired: widget.inputDialogOption.isRequired,
                isPassword: widget.inputDialogOption.isPassword,
                value: value,
              ),
            );
          },
        );
        if (res != null) {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          SharedPreferencesUtils.setPreferenceValue(
            prefs,
            widget.preferenceKey,
            res.value,
          );
          setState(
            () => value = SharedPreferencesUtils.getPreferenceValue(
              prefs,
              widget.preferenceKey,
              null,
            ),
          );
        }
      },
    );
  }
}
