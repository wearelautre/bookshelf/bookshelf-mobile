import 'package:bookshelf_mobile/task/data/models/task.dart';
import 'package:bookshelf_mobile/task/data/repository/task_repository.dart';
import 'package:bookshelf_mobile/task/exceptions/unauthorized_exception.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'task_state.dart';

class TaskCubit extends Cubit<TaskState> {
  final TasksRepository tasksRepository;

  TaskCubit({required this.tasksRepository}) : super(TaskInitial());

  void createTask(String isbn) {
    debugPrint('createTask');
    tasksRepository
        .createTask(isbn)
        .then((task) => {
      if (task != null)
        {emit(TaskCreated(task: task))}
      else
        {emit(TaskError(error: 'FAILED'))}
    })
        .catchError((error) => {
      if (error is UnauthorizedException)
        {emit(TaskError(error: error.error))}
    });
  }

  markAsRead() {
    debugPrint('markAsRead');
  }
}
