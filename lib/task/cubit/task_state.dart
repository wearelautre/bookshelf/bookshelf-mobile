part of 'task_cubit.dart';

@immutable
abstract class ApiState {}

class Unauthorized extends ApiState {
  final String error;

  Unauthorized({required this.error});
}

@immutable
abstract class TaskState {}

class TaskInitial extends TaskState {}

class TaskLoading extends TaskState {}

class TaskError extends TaskState {
  final String error;

  TaskError({required this.error});
}

class TaskCreated extends TaskState {
  final Task task;

  TaskCreated({required this.task});
}
