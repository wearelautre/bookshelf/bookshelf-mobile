import 'package:authentication_repository/authentication_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class DioClient {
  final dio = createDio();

  DioClient._internal();

  static final _singleton = DioClient._internal();

  factory DioClient() => _singleton;

  static Dio createDio() {
    const storage = FlutterSecureStorage();

    var dio = Dio(
      BaseOptions(),
    );

    dio.interceptors.addAll({
      AppInterceptors(dio, storage),
    });

    return dio;
  }
}

class AppInterceptors extends Interceptor {
  final Dio dio;
  final FlutterSecureStorage storage;

  AppInterceptors(this.dio, this.storage);

  @override
  void onRequest(
      RequestOptions options,
      RequestInterceptorHandler handler,
      ) async {
    String? idToken = await storage.read(key: AuthenticationRepository.idTokenStorageKey);
    options.headers['Authorization'] = 'Bearer $idToken';
    return handler.next(options);
  }
}

