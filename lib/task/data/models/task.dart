import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'task.g.dart';

enum TypeEnum {
  @JsonValue("ADD_BOOK")
  addBook,
  @JsonValue("OTHER")
  other,
}

enum StatusEnum {
  @JsonValue("TODO")
  todo,
  @JsonValue("DONE")
  done,
}

@JsonSerializable()
class Task extends Equatable {
  const Task({
    required this.extra,
    required this.createdDate,
    this.type = TypeEnum.addBook,
    this.status = StatusEnum.todo,
    this.id
  });

  final String extra;
  final DateTime? createdDate;
  final TypeEnum? type;
  final StatusEnum? status;
  final int? id;

  @override
  List<Object?> get props => [extra, type, status, id];

  factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);
  Map<String, dynamic> toJson() => _$TaskToJson(this);
}

