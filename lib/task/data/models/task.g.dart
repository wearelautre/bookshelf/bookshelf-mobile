// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Task _$TaskFromJson(Map<String, dynamic> json) => Task(
      extra: json['extra'] as String,
      createdDate: json['createdDate'] == null
          ? null
          : DateTime.parse(json['createdDate'] as String),
      type: $enumDecodeNullable(_$TypeEnumEnumMap, json['type']) ??
          TypeEnum.addBook,
      status: $enumDecodeNullable(_$StatusEnumEnumMap, json['status']) ??
          StatusEnum.todo,
      id: json['id'] as int?,
    );

Map<String, dynamic> _$TaskToJson(Task instance) => <String, dynamic>{
      'extra': instance.extra,
      'createdDate': instance.createdDate?.toIso8601String(),
      'type': _$TypeEnumEnumMap[instance.type],
      'status': _$StatusEnumEnumMap[instance.status],
      'id': instance.id,
    };

const _$TypeEnumEnumMap = {
  TypeEnum.addBook: 'ADD_BOOK',
  TypeEnum.other: 'OTHER',
};

const _$StatusEnumEnumMap = {
  StatusEnum.todo: 'TODO',
  StatusEnum.done: 'DONE',
};
