import 'package:bookshelf_mobile/task/data/models/task.dart';

import '../services/providers/tasks_providers.dart';

class TasksRepository {
  final TasksProviders providers;

  TasksRepository(this.providers);

  Future<Task?> createTask(String isbn) async {
    return await providers.postTask(Task(extra: isbn, createdDate: DateTime.now()));
  }
}
