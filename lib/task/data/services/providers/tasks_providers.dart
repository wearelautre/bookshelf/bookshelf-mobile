
import 'package:bookshelf_mobile/task/data/models/task.dart';
import 'package:bookshelf_mobile/task/exceptions/unauthorized_exception.dart';
import 'package:commons/settings_enum.dart';
import 'package:commons/utils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../dio_client.dart';

class TasksProviders {
  final DioClient _client = DioClient();

  TasksProviders();

  Future<Task?> postTask(Task task) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String domain = SharedPreferencesUtils.getPreferenceValue(
        prefs,
        SettingKeys.domain,
        null,
      );

      Response response = await _client.dio.post("http://$domain/tasks", data: task, options: Options(contentType: Headers.jsonContentType));
      debugPrint("SAMPLE_DATA : ${response.data}");
      return Task.fromJson(response.data);
    } on DioError catch (e) {
      if (e.response!.statusCode == 401) {
        throw UnauthorizedException(error: e.message ?? "FAILED");
      }
      debugPrint("ERROR : $e");
      return null;
    }
  }
}
