class UnauthorizedException implements Exception {
  final String error;

  UnauthorizedException({required this.error});
}
