import 'dart:async';

import 'package:authentication_repository/src/models/models.dart';
import 'package:authentication_repository/src/providers/user_provider.dart';
import 'package:cache/cache.dart';
import 'package:commons/settings_enum.dart';
import 'package:commons/utils.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum AuthenticationStatus {
  unknown,
  askForUserInfo,
  authenticated,
  unauthenticated
}

class LoginFailure implements Exception {
  const LoginFailure([
    this.message = 'An unknown exception occurred.',
  ]);

  final String message;
}

class LogOutFailure implements Exception {}

final navigatorKey = GlobalKey<NavigatorState>();

class AuthenticationRepository {
  final _controller = StreamController<AuthenticationStatus>();

  AuthenticationRepository({
    CacheClient? cache,
    FlutterAppAuth? appAuth,
    FlutterSecureStorage? storage,
    UserProvider? userProvider,
  })
      :
        _appAuth = appAuth ?? const FlutterAppAuth(),
        _cache = cache ?? CacheClient(),
        _userProvider = userProvider ?? UserProvider(),
        _storage = storage ?? const FlutterSecureStorage();

  final CacheClient _cache;
  final FlutterSecureStorage _storage;
  final UserProvider _userProvider;
  final FlutterAppAuth _appAuth;

  Stream<User> get user {
    return _controller.stream.asyncMap((status) =>
    status == AuthenticationStatus.authenticated
        ? _userProvider.getUser()
        : Future(() => User.empty));
  }

  @visibleForTesting
  static const userCacheKey = '__user_cache_key__';

  static const accessTokenStorageKey = '__accessToken_storage_key__';
  static const idTokenStorageKey = '__idToken_storage_key__';

  User get currentUser {
    return _cache.read<User>(key: userCacheKey) ?? User.empty;
  }

  Stream<AuthenticationStatus> get status async* {
    await Future<void>.delayed(const Duration(seconds: 1));
    yield AuthenticationStatus.unauthenticated;
    yield* _controller.stream;
  }

  final String _redirectUrl = 'bzh.lautre.bookshelf.app:/';
  final String _postLogoutRedirectUrl = 'bzh.lautre.bookshelf.app:/';
  final List<String> _scopes = <String>[
    'openid',
    'profile',
    'email',
    'offline_access',
  ];

  Future<void> login() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String clientId = SharedPreferencesUtils.getPreferenceValue(
      prefs,
      SettingKeys.keycloakClientId,
      null,
    );
    String clientSecret = SharedPreferencesUtils.getPreferenceValue(
      prefs,
      SettingKeys.keycloakClientSecret,
      null,
    );
    String authServerUrl = SharedPreferencesUtils.getPreferenceValue(
      prefs,
      SettingKeys.keycloakUrl,
      null,
    );
    String realm = SharedPreferencesUtils.getPreferenceValue(
      prefs,
      SettingKeys.keycloakRealm,
      null,
    );

    final String discoveryUrl = '$authServerUrl/realms/$realm/.well-known/openid-configuration';

    try {
      final AuthorizationTokenResponse? result =
      await _appAuth.authorizeAndExchangeCode(
        AuthorizationTokenRequest(
          clientId,
          _redirectUrl,
          clientSecret: clientSecret,
          discoveryUrl: discoveryUrl,
          scopes: _scopes,
        ),
      );

      if (result != null) {
        _processAuthTokenResponse(result);
      }
    } catch (e) {
      throw const LoginFailure();
    }
  }

  void _processAuthTokenResponse(AuthorizationTokenResponse response) {
    _storage.write(key: accessTokenStorageKey, value: response.accessToken!);
    _storage.write(key: idTokenStorageKey, value: response.idToken!);
    _controller.add(AuthenticationStatus.authenticated);
  }

  Future<void> logOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String authServerUrl = SharedPreferencesUtils.getPreferenceValue(
      prefs,
      SettingKeys.keycloakUrl,
      null,
    );
    String realm = SharedPreferencesUtils.getPreferenceValue(
      prefs,
      SettingKeys.keycloakRealm,
      null,
    );

    final String discoveryUrl = '$authServerUrl/realms/$realm/.well-known/openid-configuration';

    try {
      await _appAuth.endSession(EndSessionRequest(
        idTokenHint: await _storage.read(key: idTokenStorageKey),
        discoveryUrl: discoveryUrl,
        postLogoutRedirectUrl: _postLogoutRedirectUrl,
      ));
      _controller.add(AuthenticationStatus.unauthenticated);
    } catch (_) {
      throw LogOutFailure();
    }
  }

  void dispose() => _controller.close();
}
