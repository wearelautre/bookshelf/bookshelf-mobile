//providers url
import 'package:flutter_dotenv/flutter_dotenv.dart';

String apiPrefixPath = dotenv.get('API_PREFIX_PATH', fallback: '');
String apiHost = dotenv.get('API_HOST', fallback: 'http://localhost:3000');
