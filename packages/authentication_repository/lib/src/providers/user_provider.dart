import 'dart:async';

import 'package:authentication_repository/authentication_repository.dart';

class UserProvider {
  // final DioClient _dioClient = DioClient();

  Future<User> getUser() async {
    // String apiPrefixPath = dotenv.get('API_PREFIX_PATH', fallback: '');
    try {
     // Response response = await _dioClient.dio.get('$apiPrefixPath/me');
      return const User(email: "email@toto.com", name: "admin");
    } catch (error) {
      return User.empty;
    }
  }


}

class NetworkError extends Error {}