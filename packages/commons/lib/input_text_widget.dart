import 'package:commons/widget/input_dialog_option.dart';
import 'package:flutter/material.dart';

class InputReturn {
  String value;

  InputReturn({required this.value});
}

class InputTextConfiguration extends StatefulWidget {
  final InputDialogOption option;
  final Function(InputReturn) onValidate;

  const InputTextConfiguration(
      {required this.option, required this.onValidate, super.key});

  @override
  InputTextConfigurationState createState() => InputTextConfigurationState();
}

class InputTextConfigurationState extends State<InputTextConfiguration> {
  late final TextEditingController _controller;
  final String _text = '';
  bool _obscured = false;

  @override
  void initState() {
    super.initState();
    _obscured = widget.option.isPassword;
    _controller = TextEditingController(
      text: widget.option.value,
    );
  }

  String? get _errorText {
    final text = _controller.value.text;
    if (widget.option.isRequired && text.isEmpty) {
      return 'Can\'t be empty';
    }
    return null;
  }

  void _submit() {
    if (_errorText == null) {
      widget.onValidate(InputReturn(value: _controller.value.text));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Flexible(
              child: TextField(
                obscureText: _obscured,
                controller: _controller,
                onChanged: (text) => setState(() => _text),
                decoration: InputDecoration(
                  hintText: widget.option.inputHint,
                  errorText: _errorText,
                ),
              ),
            ),
            if (widget.option.isPassword) ...{
              const SizedBox(width: 5, height: 5),
              IconButton(
                onPressed: () => setState(() => _obscured = !_obscured),
                icon: Icon(
                  _obscured ? Icons.visibility_off : Icons.visibility,
                ),
              )
            },
            const SizedBox(width: 10, height: 10),
            ElevatedButton(
              onPressed: _errorText == null ? _submit : null,
              child: const Text("Validate"),
            )
          ],
        ),

      ],
    );
  }
}
