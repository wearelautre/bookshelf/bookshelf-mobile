enum SettingKeys {
  domain(token: ''),
  keycloakRealm(token: 'keycloak.domain'),
  keycloakClientId(token: 'keycloak.clientId'),
  keycloakClientSecret(token: 'keycloak.clientSecret'),
  keycloakUrl(token: 'keycloak.url'),
  ;

  const SettingKeys({required this.token});

  final String token;
}
