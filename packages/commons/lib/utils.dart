import 'package:commons/settings_enum.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesUtils {
  static dynamic getPreferenceValue(
      SharedPreferences prefs, SettingKeys key, dynamic defaultValue) {
    return prefs.containsKey(key.token) ? prefs.get(key.token) : defaultValue;
  }

  static Future<dynamic> setPreferenceValue(
    SharedPreferences prefs,
    SettingKeys key,
    dynamic value,
  ) {
    if (value is bool) {
      return prefs.setBool(key.token, value);
    } else if (value is String) {
      return prefs.setString(key.token, value);
    } else if (value is double) {
      return prefs.setDouble(key.token, value);
    } else if (value is int) {
      return prefs.setInt(key.token, value);
    } else if (value is List<String>) {
      return prefs.setStringList(key.token, value);
    } else {
      return Future.error(() => 'Wrong Type');
    }
  }
}
