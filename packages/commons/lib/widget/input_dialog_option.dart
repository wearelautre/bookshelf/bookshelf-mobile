class InputDialogOption {
  String titre;
  String inputHint;
  String? value;
  bool isRequired;
  bool isPassword;

  InputDialogOption({
    required this.titre,
    required this.inputHint,
    this.value,
    this.isRequired = false,
    this.isPassword = false,
  });
}
