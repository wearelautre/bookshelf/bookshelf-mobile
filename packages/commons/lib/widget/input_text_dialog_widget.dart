import 'package:commons/widget/input_dialog_option.dart';
import 'package:flutter/material.dart';

class InputDialogReturn {
  String value;

  InputDialogReturn({required this.value});
}

class InputDialog extends StatefulWidget {
  final InputDialogOption option;

  const InputDialog({required this.option, super.key});

  @override
  InputDialogState createState() => InputDialogState();
}

class InputDialogState extends State<InputDialog> {
  late final TextEditingController _controller;
  final String _text = '';
  bool _obscured = false;

  @override
  void initState() {
    super.initState();
    _obscured = widget.option.isPassword;
    _controller = TextEditingController(
      text: widget.option.value,
    );
  }

  String? get _errorText {
    final text = _controller.value.text;
    if (widget.option.isRequired && text.isEmpty) {
      return 'Can\'t be empty';
    }
    return null;
  }

  void _submit() {
    if (_errorText == null) {
      Navigator.of(context).pop(
        InputDialogReturn(value: _controller.value.text),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.option.titre),
      content: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Flexible(
            child: TextField(
              obscureText: _obscured,
              controller: _controller,
              onChanged: (text) => setState(() => _text),
              decoration: InputDecoration(
                hintText: widget.option.inputHint,
                errorText: _errorText,
              ),
            ),
          ),
          if (widget.option.isPassword) ...{
            const SizedBox(width: 5, height: 5),
            IconButton(
              onPressed: () => setState(() => _obscured = !_obscured),
              icon: Icon(
                _obscured ? Icons.visibility_off : Icons.visibility,
              ),
            )
          },
        ],
      ),
      actions: [
        ElevatedButton(
          onPressed: () => Navigator.pop(context),
          style: ElevatedButton.styleFrom(backgroundColor: Colors.red),
          child: const Text("Cancel"),
        ),
        ElevatedButton(
          onPressed: _errorText == null ? _submit : null,
          child: const Text("Validate"),
        )
      ],
    );
  }
}
