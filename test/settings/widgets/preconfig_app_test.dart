import 'package:bookshelf_mobile/preconfig_app.dart';
import 'package:commons/settings_enum.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  group('PreconfigApp', () {
    testWidgets('nothing configured', (tester) async {
      SharedPreferences.setMockInitialValues({});

      SharedPreferences prefs = await SharedPreferences.getInstance();

      await tester.pumpWidget(
        PreconfigApp(
            prefs: prefs, onConfigurationUpdate: () => debugPrint('object')),
      );

      final widgetKeyDomain = find.byKey(const Key("Domain"));
      final widgetKeyServerUrl = find.byKey(const Key("Auth server url (keycloak)"));
      final widgetKeyRealm = find.byKey(const Key("Realm"));
      final widgetKeyClientId = find.byKey(const Key("Client ID"));
      final widgetKeyClientSecret = find.byKey(const Key("Client secret"));

      expect(widgetKeyDomain, findsOneWidget);
      expect(widgetKeyServerUrl, findsOneWidget);
      expect(widgetKeyRealm, findsOneWidget);
      expect(widgetKeyClientId, findsOneWidget);
      expect(widgetKeyClientSecret, findsOneWidget);
    });
    testWidgets('domain configured', (tester) async {
      SharedPreferences.setMockInitialValues({SettingKeys.domain.token: "configured"});

      SharedPreferences prefs = await SharedPreferences.getInstance();

      await tester.pumpWidget(
        PreconfigApp(
            prefs: prefs, onConfigurationUpdate: () => debugPrint('object')),
      );

      final widgetKeyDomain = find.byKey(const Key("Domain"));
      final widgetKeyServerUrl = find.byKey(const Key("Auth server url (keycloak)"));
      final widgetKeyRealm = find.byKey(const Key("Realm"));
      final widgetKeyClientId = find.byKey(const Key("Client ID"));
      final widgetKeyClientSecret = find.byKey(const Key("Client secret"));

      expect(widgetKeyDomain, findsNothing);
      expect(widgetKeyServerUrl, findsOneWidget);
      expect(widgetKeyRealm, findsOneWidget);
      expect(widgetKeyClientId, findsOneWidget);
      expect(widgetKeyClientSecret, findsOneWidget);
    });
    testWidgets('ClientId configured', (tester) async {
      SharedPreferences.setMockInitialValues({SettingKeys.keycloakClientId.token: "configured"});

      SharedPreferences prefs = await SharedPreferences.getInstance();

      await tester.pumpWidget(
        PreconfigApp(
            prefs: prefs, onConfigurationUpdate: () => debugPrint('object')),
      );

      final widgetKeyDomain = find.byKey(const Key("Domain"));
      final widgetKeyServerUrl = find.byKey(const Key("Auth server url (keycloak)"));
      final widgetKeyRealm = find.byKey(const Key("Realm"));
      final widgetKeyClientId = find.byKey(const Key("Client ID"));
      final widgetKeyClientSecret = find.byKey(const Key("Client secret"));

      expect(widgetKeyDomain, findsOneWidget);
      expect(widgetKeyServerUrl, findsOneWidget);
      expect(widgetKeyRealm, findsOneWidget);
      expect(widgetKeyClientId, findsNothing);
      expect(widgetKeyClientSecret, findsOneWidget);
    });
    testWidgets('ClientSecret configured', (tester) async {
      SharedPreferences.setMockInitialValues({SettingKeys.keycloakClientSecret.token: "configured"});

      SharedPreferences prefs = await SharedPreferences.getInstance();

      await tester.pumpWidget(
        PreconfigApp(
            prefs: prefs, onConfigurationUpdate: () => debugPrint('object')),
      );

      final widgetKeyDomain = find.byKey(const Key("Domain"));
      final widgetKeyServerUrl = find.byKey(const Key("Auth server url (keycloak)"));
      final widgetKeyRealm = find.byKey(const Key("Realm"));
      final widgetKeyClientId = find.byKey(const Key("Client ID"));
      final widgetKeyClientSecret = find.byKey(const Key("Client secret"));

      expect(widgetKeyDomain, findsOneWidget);
      expect(widgetKeyServerUrl, findsOneWidget);
      expect(widgetKeyRealm, findsOneWidget);
      expect(widgetKeyClientId, findsOneWidget);
      expect(widgetKeyClientSecret, findsNothing);
    });
    testWidgets('Url configured', (tester) async {
      SharedPreferences.setMockInitialValues({SettingKeys.keycloakUrl.token: "configured"});

      SharedPreferences prefs = await SharedPreferences.getInstance();

      await tester.pumpWidget(
        PreconfigApp(
            prefs: prefs, onConfigurationUpdate: () => debugPrint('object')),
      );

      final widgetKeyDomain = find.byKey(const Key("Domain"));
      final widgetKeyServerUrl = find.byKey(const Key("Auth server url (keycloak)"));
      final widgetKeyRealm = find.byKey(const Key("Realm"));
      final widgetKeyClientId = find.byKey(const Key("Client ID"));
      final widgetKeyClientSecret = find.byKey(const Key("Client secret"));

      expect(widgetKeyDomain, findsOneWidget);
      expect(widgetKeyServerUrl, findsNothing);
      expect(widgetKeyRealm, findsOneWidget);
      expect(widgetKeyClientId, findsOneWidget);
      expect(widgetKeyClientSecret, findsOneWidget);
    });
    testWidgets('Realm configured', (tester) async {
      SharedPreferences.setMockInitialValues({SettingKeys.keycloakRealm.token: "configured"});

      SharedPreferences prefs = await SharedPreferences.getInstance();

      await tester.pumpWidget(
        PreconfigApp(
            prefs: prefs, onConfigurationUpdate: () => debugPrint('object')),
      );

      final widgetKeyDomain = find.byKey(const Key("Domain"));
      final widgetKeyServerUrl = find.byKey(const Key("Auth server url (keycloak)"));
      final widgetKeyRealm = find.byKey(const Key("Realm"));
      final widgetKeyClientId = find.byKey(const Key("Client ID"));
      final widgetKeyClientSecret = find.byKey(const Key("Client secret"));

      expect(widgetKeyDomain, findsOneWidget);
      expect(widgetKeyServerUrl, findsOneWidget);
      expect(widgetKeyRealm, findsNothing);
      expect(widgetKeyClientId, findsOneWidget);
      expect(widgetKeyClientSecret, findsOneWidget);
    });
  });
}
